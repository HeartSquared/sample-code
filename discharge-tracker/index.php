<?php
/* Cassandra Tam 2018 */

if (PHP_SESSION_NONE == session_status()) { session_start(); }

$no_redirect = TRUE;
$base_path = basename(__DIR__);

if (!isset($_SESSION['logged_in'])) {
  $_SESSION['logged_in'] = FALSE;
}

require_once('includes/common.php');

// Go to users.php if already logged in.
if ($logged_in) {
  header('Location: users.php');
}

// Error setting for if Javascript is not enabled.
$error_hidden = TRUE;
$error_message = '';
require_once('includes/login.php');

?>
<!DOCTYPE html>
<html>

  <head>
    <?php include('includes/incl-head.html'); ?>
    <title>Discharge Tracker - Login</title>
  </head>

  <body class="container">
    <!-- Navigation Bar -->
    <?php include('includes/nav.php'); ?>

    <!-- Main Content -->
    <div class="container">

      <!-- Page Title -->
      <div class="row">
        <section class="col-xs-12">
          <h3>Login</h3>
        </section>
      </div>

      <!-- Content -->
      <div class="row">

        <?php if (!file_exists('includes/database')): ?>
          <div class="col-xs-12">
            Database is missing. Please run the <a href="install.php">install</a> file first.
          </div>
        <?php else: ?>
          <!-- Error -->
          <section id="error" class="col-sm-6 col-xs-12 pull-right
            <?php if ($error_hidden): ?>
              hidden
            <?php endif; ?>
          ">
            <div class="alert alert-danger col-xs-12">
              <p id="error_message">
                <?php if (isset($error_message)): ?>
                    <?php echo $error_message; ?>
                <?php endif; ?>
              </p>
            </div>
          </section>

          <!-- Login Form -->
          <section class="col-sm-6 col-xs-12">
            <form id="form_login" method="post" action="" class="form-horizontal">

              <!-- Input: Username -->
              <div class="form-group">
                <label for="username" class="control-label col-xs-12">Username:</label>
                <div class="col-xs-12">
                  <input type="text" id="username" name="username" class="form-control" placeholder="Enter username" required
                    <?php if (isset($_POST['username'])): ?>
                      <?php echo ' value=' . $_POST['username']; ?>
                    <?php endif; ?>
                  >
                </div>
              </div>

              <!-- Input: Password -->
               <div class="form-group">
                <label for="password" class="control-label col-xs-12">Password:</label>
                <div class="col-xs-12">
                  <input type="password" id="password" name="password" class="form-control" placeholder="Enter password" required>
                </div>
              </div>

              <!-- Submit Button: Login -->
              <div class="form-group">
                <div class="col-xs-12">
                  <input type="submit" name="btn_login" class="btn btn-default pull-right col-sm-2 col-xs-12" value="Login">
                </div>
              </div>

            </form>
          </section>
        <?php endif; ?>
      </div><!-- /.row -->
    </div><!-- /.container -->

    <!-- Footer -->
    <?php include('includes/footer.php'); ?>

    <!-- Scripts -->
    <?php include('includes/incl-js.html'); ?>
    <script>
      $(document).ready(function(){
        $('#form_login').submit(function(e){
          //Stop the form from submitting itself to the server.
          e.preventDefault();
          // Assign input values to variables.
          var username = $('#username').val();
          var password = $('#password').val();

          var data = {
            username: username,
            password: password
          };
          // Pass data to ajax form.
          $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'includes/login.php',
            data: {
              json_data: JSON.stringify(data)
            },
            success: function(result) {
              // No errors.
              if (true == result.success) {
                $(location).attr('href', 'users.php');
              }
              // Error occurred (username/password didn't match).
              else {
                // Display messages.
                $('#error').removeClass('hidden');
                $('#error_message').html(result.error);
              }
            }
          });
        });
      });
    </script>

  </body>
</html>
