<?php
/* Cassandra Tam 2018 */

// Decode POST data.
if (isset($_POST)) {
  // Javascript is enabled.
  if (isset($_POST['json_data'])) {
    $no_redirect = TRUE;
    $base_path = basename(__DIR__);

    require_once('common.php');

    $js_enabled = TRUE;
    $post_data = json_decode($_POST['json_data'], TRUE);
  }
  // Fallback - Javascript is disabled.
  else {
    $js_enabled = FALSE;
    $post_data = $_POST;
  }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_login']) || $js_enabled) {

  $login_user = $post_data['username'];
  $login_pass = $post_data['password'];

  $usernames_to_check = [$login_user];

  // Allow not needing to enter config domain.
  // Check entered username does not contain the domain entered in config file.
  if (!preg_match("/{$config['domain']}$/", $login_user)) {
    // Appends config domain to username and adds to usernames to check.
    $usernames_to_check[] = $login_user . '@' . $config['domain'];
  }

  // Loop through usernames.
  foreach ($usernames_to_check as $username) {
    $credential = $container->getCredentialLoader()->getCredential($username);
    // Stop checking through array if we find a match.
    if (!empty($credential)) {
      break;
    }
  }

  $invalid_login = FALSE;

  // No such user in database.
  if (empty($credential)) {
    $invalid_login = TRUE;
  }
  // User in database.
  else {
    $uid = $credential->getUid();
    $password = $credential->getPassword();

    // Password does not match.
    if (!password_verify($login_pass, $password)) {
      $invalid_login = TRUE;
    }
  }

  // Invalid login.
  if ($invalid_login) {
    $result = [
      'success' => FALSE,
      'error' => 'Invalid username or password.',
    ];
  }
  else {
    // Assign session variables.
    $_SESSION['current_uid'] = $uid;
    $_SESSION['logged_in'] = TRUE;

    $result = [
      'success' => TRUE,
    ];
  }

  // Javascript is enabled.
  if ($js_enabled) {
    // Return JSON encoded result.
    echo json_encode($result);
  }
  // Javascript is disabled.
  else {
    // Successful.
    if (TRUE == $result['success']) {
        header('Location: users.php');
    }
    // Failed. Display errors.
    else {
      $error_hidden = FALSE;
      $error_message = $result['error'];
    }
  }

}
