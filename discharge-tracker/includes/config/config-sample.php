<?php

// Replace with your pharmacy details.
// Pharmacy name.
$pharmacy_name = 'Example Pharmacy';
// Domain name used to assist in email pre-fills when registering new users.
$website_domain = 'examplepharmacy.test';
$database_name = 'example-discharge-tracker';

// Replace with the details of your system administrator.
$sysadmin_firstname = 'FIRSTNAME';
$sysadmin_lastname = 'LASTNAME';
$sysadmin_email = 'admin@examplepharmacy.test';

/**
 *
 *
 *
 * !! DO NOT EDIT BELOW THIS POINT !!
 *
 *
 *
 */

define('PROJECT_ROOT', dirname(dirname(__DIR__)) . '/');

$config = [];

// Pharmacy details.
$config['name'] = $pharmacy_name;
$config['domain'] = $website_domain;

// Database details.
$config['database'] = [
  'name' => $database_name,
];

// Details for System Administrator.
$config['sysadmin'] = [
  'firstname' => $sysadmin_firstname,
  'lastname' => $sysadmin_lastname,
  'email' => strtolower($sysadmin_email),
];

// Path to database (relative to root).
$config['database']['dsn'] = 'sqlite:' . PROJECT_ROOT . 'includes/database/' . $config['database']['name'] . '.sq3';
