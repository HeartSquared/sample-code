<?php
/* Cassandra Tam 2017 */

if (!session_start()) { session_start(); }

$id = $_POST['id'];
$type = $_POST['type'];

// Assign id to session variable and forward to editing page.
switch ($type) {

  case 'users':
    $_SESSION['editing_uid'] = $id;
    header('Location: ../edit-user.php');
    break;

  default:
    header('Location: ../users.php');
}
