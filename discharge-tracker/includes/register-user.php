<?php
/* Cassandra Tam 2018 */

// Decode POST data.
if (isset($_POST)) {
  // Javascript is enabled.
  if (isset($_POST['json_data'])) {
    $base_path = basename(__DIR__);

    require_once('common.php');

    $js_enabled = TRUE;
    // Decode POST data and trim the values.
    $post_data = trim_array_values(json_decode($_POST['json_data'], TRUE));
  }

  // Fallback - Javascript is disabled.
  else {
    $js_enabled = FALSE;
    $post_data = trim_array_values($_POST);

    $field_ids = [
      'firstname',
      'lastname',
      'email',
      'role',
    ];

    // If the $post_data array doesn't currently have the field, set it to null.
    foreach ($field_ids as $id) {
      if (!isset($post_data[$id])) {
        $post_data[$id] = NULL;
      }
    }

  }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_register']) || $js_enabled) {

  $credential_editor = $container->getCredentialEditor();
  $user_editor = $container->getUserEditor();

  $post_data['email'] = strtolower($post_data['email']);

  $errors = [];
  $error_fields = [];

  // Fields to perform error checking on.
  // Key: Field id.
  // Value: Field label name for error message.
  $fields_to_validate = [
    'firstname' => 'First Name',
    'lastname' => 'Last Name',
    'email' => 'Email',
    'role' => 'User Role',
  ];

  foreach ($fields_to_validate as $field_id => $label) {
    // Reset each field to not have error class.
    $errors['fields'][$field_id] = FALSE;

    // Input is blank.
    if (empty($post_data[$field_id])) {
      $errors['messages'][] = $label . ' cannot be blank.';
      $error_fields[] = $field_id;
    }

    if ('email' == $field_id) {
      // Check email format.
      $valid_email = filter_var($post_data[$field_id], FILTER_VALIDATE_EMAIL);
      if (!$valid_email) {
        $errors['messages'][] = $label . ' is not in the correct format.';
        $error_fields[] = $field_id;
      }

      // Check if credential with matching username (email) exists.
      $user_exists = $credential_editor->checkUserExists($post_data['email']);
      // Username (email) in use.
      if ($user_exists) {
        $errors['messages'][] = $label . ' is already in use.';
        $error_fields[] = $field_id;
      }
    }
    // Check for invalid characters.
    else {
      $has_invalid_char = check_invalid_characters($post_data[$field_id]);
    }

    if ($has_invalid_char) {
      $errors['messages'][] = $label . ' contains invalid characters.';
      $error_fields[] = $field_id;
    }
  }

  // Give error class to fields with errors.
  foreach (array_unique($error_fields) as $error_field) {
    if (in_array($error_field, array_keys($fields_to_validate))) {
      $errors['fields'][$error_field] = TRUE;
    }
  }

  // Check if there are errors.
  if (!empty($errors['messages'])) {
    $result = array_merge(['success' => FALSE], ['errors' => $errors]);
  }
  // No errors.
  else {
    // Create new login credentials.
    $credential_editor->createCredential($post_data['email'], Model\AbstractUserType::USERTYPE_USER);

    // Retrieve matching user login credentials.
    $uid = $credential_editor->getCredential($post_data['email'])->getUid();

    // Register User profile.
    $user_editor->createUser($uid, $post_data['firstname'], $post_data['lastname'], $post_data['role'], $post_data['email']);

    $result = ['success' => TRUE];
  }

  // Javascript is enabled.
  if ($js_enabled) {
    // Return JSON encoded result.
    echo json_encode($result);
  }
  // Javascript is disabled.
  else {
    // Successful.
    if (TRUE == $result['success']) {
      header('Location: users.php');
    }
    // Failed. Display errors.
    else {
      $error_hidden = FALSE;
      $error_message = '';
      foreach ($result['errors']['messages'] as $error) {
        $error_message .= "$error<br>";
      }
    }
  }

}
