<?php

$base_path = basename(__DIR__);

require_once('common.php');

$table_manager = $container->getTableManager();

// Decode GET data.
$json_decoded = json_decode($_GET['json_data'], TRUE);

switch ($json_decoded['collection_type']) {

  case 'users':
    $table = $container->getTableUsers($user);
    break;
}

$table_manager->displayTableData($table, $json_decoded['collection_type'], $json_decoded['sort_option'], $json_decoded['sort_order']);
