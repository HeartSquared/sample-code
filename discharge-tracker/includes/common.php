<?php

// Start session.
if (PHP_SESSION_NONE == session_status()) { session_start(); }

// Get logged in status.
$logged_in = FALSE;

if (TRUE == $_SESSION['logged_in']) {
  $logged_in = TRUE;
}

// Return to index if not logged in.
if ((!$logged_in) && (TRUE != $no_redirect)) {
  header('Location: index.php');
}

$base_common = basename(__DIR__);
if ($base_common == $base_path) {
  require_once('config/config.php');
}
else {
  require_once('includes/config/config.php');
}

require_once(PROJECT_ROOT . 'includes/functions.php');
require_once(PROJECT_ROOT . 'includes/permissions.php');

// Autoload classes.
spl_autoload_register(function ($class) {

  $path = PROJECT_ROOT . 'classes/'. str_replace('\\', '/', $class) . '.php';

  if (file_exists($path)) {
    require $path;
  }

});

// Unset session items which should only be set on their corresponding pages.
unset_session_items();

use Model\AbstractUserType;
use Service\Container;
$container = new Container($config['database']);

if ($logged_in) {
  $current_uid = $_SESSION['current_uid'];
  $user_credential = $container->getCredentialLoader()->getCredentialById($current_uid);
  if (AbstractUserType::USERTYPE_USER == $user_credential->getUserType()) {
    $user = $container->getUserLoader()->getUserById($current_uid);
  }
}
