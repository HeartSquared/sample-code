<?php
  use Model\AbstractUserType;

  require_once('common.php');

  $is_type_user = FALSE;
  $is_pharmacy = FALSE;
  $is_manager = FALSE;
  $is_admin = FALSE;
  // Check logged in user access.
  if ($logged_in) {
    $is_type_user = check_access(get_roles_with_access('is type user'), $user);
    $is_pharmacy = check_access(get_roles_with_access('is pharmacy'), $user);
    $is_manager = check_access(get_roles_with_access('is manager'), $user);
    $is_admin = check_access(get_roles_with_access('is admin'), $user);
  }
?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <!-- *LOGGED IN nav* -->
      <?php if ($logged_in): ?>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
        </button>
      <?php endif; ?>
      <a class="navbar-brand" href="index.php" style="color:#f59654;">Discharge Tracker</a>
    </div><!-- /.navbar-header -->

    <!-- *LOGGED IN nav* -->
    <?php if ($logged_in): ?>
      <div class="collapse navbar-collapse" id="navbar">
        <!-- LEFT -->
        <ul class="nav navbar-nav">
          <!-- ADMIN/MANAGER nav -->
          <?php if ($is_manager): ?>
            <li class="dropdown">
              <li><a href="register-user.php">Register User</a></li>
              <li><a href="users.php">View/Edit User</a></li>
            </li>
          <?php endif; ?><!-- end ADMIN/MANAGER nav -->
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <?php echo $user_credential->getUsername(); ?>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="includes/logout.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /#navbar -->
    <?php endif; ?>
  </div><!-- /.container-fluid -->
</nav>
