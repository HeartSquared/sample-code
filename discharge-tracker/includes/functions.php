<?php

use Model\AbstractUserType;

/**
 * Check if the user has access to the restricted areas.
 *
 * @param array $roles
 * @param User $user
 *
 * @return bool
 */
function check_access(array $roles, AbstractUserType $user = NULL) {
  if (isset($user) && in_array($user->getRole(), $roles)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Unset session items which should be confined to their corresponding pages.
 */
function unset_session_items() {
  // Get the current page file name.
  $filename = basename($_SERVER['PHP_SELF']);

  if ('edit-user.php' != $filename) {
    unset($_SESSION['editing_uid']);
  }
}

/**
 * Trim values and return the new array.
 *
 * @param array $array
 *
 * @return array
 */
function trim_array_values(array $array) {
  $new_array = [];

  foreach ($array as $key => $value) {
    // If there's a value, return it trimmed.
    if (isset($value) && !is_null($value)) {
      $new_array[$key] = trim($value);
    }
    // No value, then set it to NULL.
    else {
      $new_array[$key] = NULL;
    }
  }

  return $new_array;
}

/**
 * Check for special characters.
 *
 * @param string $input
 *
 * @return bool
 */
function check_invalid_characters($input, $allow_spaces = TRUE) {
  if ($allow_spaces) {
    return (preg_match("/[^A-Za-z0-9 ]/", $input));
  }
  else {
    return (preg_match("/[^A-Za-z0-9]/", $input));
  }
}
