<?php
/* Cassandra Tam 2018 */

$base_path = basename(__DIR__);

require_once('includes/common.php');

use Service\UserLoader;

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

// Error setting for if Javascript is not enabled.
$error_hidden = TRUE;
$error_message = '';
require_once('includes/register-user.php');

// Value filling for if Javascript is not enabled.
$firstname_value = isset($_POST['firstname']) ? $_POST['firstname'] : NULL;
$lastname_value = isset($_POST['lastname']) ? $_POST['lastname'] : NULL;
$email_value = isset($_POST['email']) ? $_POST['email'] : NULL;
$role_value = isset($_POST['role']) ? $_POST['role'] : NULL;
?>

<!DOCTYPE html>
<html>

  <head>
    <?php include('includes/incl-head.html'); ?>
    <title>Discharge Tracker - Register User</title>
    <script src="resources/angular.min.js"></script>
    <script>
      var app = angular.module('regUser', []);

      var domain_name = <?php echo json_encode($config['domain']); ?>;

      app.controller('formController', function($scope) {
        $scope.domain_name = '@' + domain_name;
      });
    </script>
  </head>

  <body>
    <!-- Navigation Bar -->
    <?php include('includes/nav.php'); ?>

    <!-- Main Content -->
    <div class="container">

      <?php if (!$has_access): ?>
        <?php include('includes/forbidden.html'); ?>
      <?php else: ?>
        <!-- Page Title -->
        <div class="row">
          <section class="col-xs-12">
            <h3>Register User</h3>
          </section>
        </div>

        <!-- Content -->
        <div class="row">

          <!-- Register Form -->
          <form id="form_register" method="post" action="" class="form-horizontal">
            <section class="col-sm-6 col-xs-12" ng-app="regUser" ng-controller="formController">

              <!-- Input: First Name -->
              <div class="form-group">
                <label for="firstname" class="control-label col-xs-12">First Name:</label>
                <div class="col-xs-12">
                  <input type="text" id="firstname" name="firstname" class="form-control" ng-model="firstname" placeholder="required" value="<?php echo $firstname_value; ?>" required>
                </div>
              </div>

              <!-- Input: Last Name -->
              <div class="form-group">
                <label for="lastname" class="control-label col-xs-12">Last Name:</label>
                <div class="col-xs-12">
                  <input type="text" id="lastname" name="lastname" class="form-control" ng-model="lastname" placeholder="required" value="<?php echo $lastname_value; ?>" required>
                </div>
              </div>

              <!-- Email -->
              <div class="form-group">
                <label for="email" class="control-label col-xs-12">Email:</label>
                <div class="col-xs-12">
                  <!-- Javascript disabled -->
                  <div id="field_email_no_js">
                    <input type="text" name="email" class="form-control" placeholder="required" value="<?php echo $email_value; ?>" required>
                  </div>
                  <!-- Javascript enabled -->
                  <div id="field_email_js" class="input-group hidden">
                    <input type="text" id="email" class="form-control" value='{{firstname + "." + lastname + domain_name | lowercase}}' disabled>
                    <span id="btn_edit_email" class="input-group-btn">
                      <button type="button" class="btn btn-default">Edit</button>
                    </span>
                  </div>
                </div>
              </div>

              <!-- Input: User Role -->
              <div class="form-group">
                <label for="role" class="control-label col-xs-12">User Role:</label>
                <div class="col-sm-6 col-xs-12">
                  <select id="role" name="role" class="form-control">
                    <option style="display:none;" value disabled selected> - - Select a role - - </option>
                    <?php
                      // Get User Roles options.
                      $user_roles = $container->getUserLoader()->getUserRoles();
                      $admin_roles = [$user_roles['sysadmin'], $user_roles['admin']];

                      // Only System Administrators can give others admin roles.
                      if ($user_roles['sysadmin'] != $user->getRole()) {
                        $user_roles = array_diff($user_roles, $admin_roles);
                      }

                      // Output data of each row
                      foreach ($user_roles as $role) {
                        echo '<option value="' . $role. '"';
                        if ($role == $role_value) {
                          echo ' selected';
                        }
                        echo ">$role</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>

            </section>

            <!-- Error -->
            <section id="error" class="col-sm-6 col-xs-12
              <?php if ($error_hidden): ?>
                hidden
              <?php endif; ?>
            ">
              <div class="alert alert-danger col-xs-12">
                <p id="error_message">
                  <?php if (isset($error_message)): ?>
                    <?php echo $error_message; ?>
                  <?php endif; ?>
                </p>
              </div>
            </section>

            <!-- Submit Button: Register -->
            <section class="col-sm-6 col-xs-12">
              <input type="submit" name="btn_register" class="btn btn-success col-xs-12" value="Register">
            </section>
          </form>

        </div><!-- /.row -->
      <?php endif; ?>
    </div><!-- /.container -->

    <!-- Footer -->
    <?php include('includes/footer.php'); ?>

    <!-- Scripts -->
    <?php include('includes/incl-js.html'); ?>
    <script>
      $(document).ready(function(){

        // Change the shown email field if JS is enabled.
        $('#field_email_no_js').html('');
        $('#field_email_js').removeClass('hidden');

        // Enable manual editing of email
        $('#btn_edit_email').click(function() {
          $('#email').removeAttr('disabled');
          $('#email').parent().removeClass('input-group');
          $('#btn_edit_email').addClass('hidden');
        });

        $('#form_register').submit(function(e){
          //Stop the form from submitting itself to the server.
          e.preventDefault();
          // Assign input values to variables.
          var firstname = $('#firstname').val();
          var lastname = $('#lastname').val();
          var email = $('#email').val();
          var role = $('#role').val();

          var data = {
            firstname: firstname,
            lastname: lastname,
            email: email,
            role: role
          };

          // Pass data to ajax form.
          $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'includes/register-user.php',
            data: {
              json_data: JSON.stringify(data)
            },
            success: function(result) {
              // No errors.
              if (true == result.success) {
                $(location).attr('href', 'users.php');
              }
              // Error occurred.
              else {
                errors = result.errors;
                messages = '';
                // Add breaks between each message.
                for (var key in errors.messages) {
                  messages += errors.messages[key] + '<br>';
                }
                // Adds/removes .has-error from fields.
                for (var key in errors.fields) {
                  if (errors.fields[key]) {
                    $('#' + key).parent().addClass('has-error');
                  }
                  else {
                    $('#' + key).parent().removeClass('has-error');
                  }
                }
                // Display messages.
                $('#error').removeClass('hidden');
                $('#error_message').html(messages);
              }
            }
          });
        });
      });
    </script>

  </body>
</html>
