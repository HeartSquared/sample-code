<?php
/* Cassandra Tam 2018 */

$base_path = basename(__DIR__);

require_once('includes/common.php');

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

$table_manager = $container->getTableManager();
$table = $container->getTableUsers($user);

$collection_type = 'users';

// Set table sorting default.
$sort_default = 'role';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php include('includes/incl-head.html'); ?>
    <title>Discharge Tracker - Users</title>
  </head>

  <body>
    <!-- Navigation Bar -->
    <?php include('includes/nav.php'); ?>

    <!-- Main Content -->
    <div class="container">

      <?php if (!$has_access): ?>
        <?php include('includes/forbidden.html'); ?>
      <?php else: ?>
        <!-- Page Title -->
        <div class="row">
          <section class="col-xs-12">
            <h3>
              Users
            </h3>
          </section>
        </div>

        <!-- Content -->
        <div class="row">

          <!-- Table -->
          <section class="col-xs-12">
            <?php $table_manager->displayTable($table, $collection_type, $sort_default); ?>
          </section>

        </div><!-- /.row -->
      <?php endif; ?>
    </div><!-- /.container -->

    <!-- Footer -->
    <?php include('includes/footer.php'); ?>

    <!-- Scripts -->
    <?php
      include('includes/incl-js.html');
      include('includes/common-table.html');
    ?>

  </body>
</html>
