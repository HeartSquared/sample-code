<?php
/* Cassandra Tam 2018 */

$base_path = basename(__DIR__);

require_once('includes/common.php');

use Service\UserLoader;

// Check if there is an entry to edit.
if (!isset($_SESSION['editing_uid'])) { header('Location: users.php'); }

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

$credential_editor = $container->getCredentialEditor();
$user_editor = $container->getUserEditor();

// Get editing user details.
$uid = $_SESSION['editing_uid'];
$account = $user_editor->getUserById($uid);

// Get automatically generated email based on user's name.
$generated_email = "{$account->getFirstName()}.{$account->getLastName()}@{$config['domain']}";
$generated_email = strtolower($generated_email);

// Error setting for if Javascript is not enabled.
$error_hidden = TRUE;
$error_message = '';
require_once('includes/edit-user.php');

// Value filling for if Javascript is not enabled.
$firstname_value = isset($_POST['firstname']) ? $_POST['firstname'] : $account->getFirstName();
$lastname_value = isset($_POST['lastname']) ? $_POST['lastname'] : $account->getLastName();
$email_value = isset($_POST['email']) ? $_POST['email'] : $account->getEmail();
$role_value = isset($_POST['role']) ? $_POST['role'] : $account->getRole();
?>

<!DOCTYPE html>
<html>

  <head>
    <?php include('includes/incl-head.html'); ?>
    <title>Discharge Tracker - Edit User</title>
    <script src="resources/angular.min.js"></script>
    <script>
      var app = angular.module('editUser', []);

      var firstname = <?php echo json_encode($account->getFirstName()); ?>;
      var lastname = <?php echo json_encode($account->getLastName()); ?>;
      var domain_name = <?php echo json_encode($config['domain']); ?>;

      app.controller('formController', function($scope) {
        $scope.firstname = firstname;
        $scope.lastname = lastname;
        $scope.domain_name = '@' + domain_name;
      });
    </script>

  </head>

  <body>
    <!-- Navigation Bar -->
    <?php include('includes/nav.php'); ?>

    <!-- Main Content -->
    <div class="container">

      <?php
        // Check if the logged user does not have access, or if they do not
        // manage the selected user.
        if (!$has_access):
      ?>
        <?php include('includes/forbidden.html'); ?>
      <?php else: ?>
        <!-- Page Title -->
        <div class="row">
          <section class="col-xs-12">
            <h3>Edit User</h3>
          </section>
        </div>

        <!-- Content -->
        <!-- Warning -->
        <?php if (1 == $uid): ?>
          <div class="row">
            <section class="col-xs-12">
              <div class="alert alert-warning col-xs-12">
                <p><strong>Note!</strong> This is User 1 (System Admin).</p>
              </div>
            </section>
          </div>
        <?php endif; ?>

        <div class="row">

          <!-- Edit Form -->
          <form id="form_edit" method="post" action="" class="form-horizontal">
            <section class="col-sm-6 col-xs-12" ng-app="editUser" ng-controller="formController">

              <!-- Input: First Name -->
              <div class="form-group">
                <label for="firstname" class="control-label col-xs-12">First Name:</label>
                <div class="col-xs-12">
                  <input type="text" id="firstname" name="firstname" class="form-control" ng-model="firstname" ng-value="firstname" placeholder="required" value="<?php echo $firstname_value; ?>" required>
                </div>
              </div>

              <!-- Input: Last Name -->
              <div class="form-group">
                <label for="lastname" class="control-label col-xs-12">Last Name:</label>
                <div class="col-xs-12">
                  <input type="text" id="lastname" name="lastname" class="form-control" ng-model="lastname" ng-value="lastname" placeholder="required" value="<?php echo $lastname_value; ?>" required>
                </div>
              </div>

              <!-- Email -->
              <div class="form-group">
                <label for="email" class="control-label col-xs-12">Email:</label>
                <div class="col-xs-12">
                  <?php if ($generated_email != $account->getEmail()): ?>
                    <input type="text" id="email" name="email" class="form-control" value="<?php echo $email_value; ?>" required>
                  <?php else: ?>
                    <div id="field_email_no_js">
                      <input type="text" name="email" class="form-control" value="<?php echo $email_value; ?>" required>
                    </div>
                    <div id="field_email_js" class="hidden">
                      <div class="input-group">
                        <input id="email" type="text" class="form-control" value='{{firstname + "." + lastname + domain_name | lowercase}}' disabled required>
                        <span id="btn_edit_email" class="input-group-btn">
                          <button type="button" class="btn btn-default">Edit</button>
                        </span>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
              </div>

              <!-- Input: User Role -->
              <div class="form-group">
                <label for="role" class="control-label col-xs-12">User Role:</label>
                <div class="col-sm-6 col-xs-12">
                  <?php
                    // Get User Roles options.
                    $user_roles = $container->getUserLoader()->getUserRoles();
                  ?>
                  <!-- Editing User 1 -->
                  <?php if (1 == $uid): ?>
                    <!-- Disable editing  -->
                    <select id="role" name="role" class="form-control" disabled required>
                      <option value="<?php echo $user_roles['sysadmin'] ?>" selected>
                        <?php echo $user_roles['sysadmin'] ?>
                      </option>
                    </select>

                  <?php else: ?>
                    <select id="role" name="role" class="form-control" <?php if (1 == $uid) { echo 'disabled'; } ?> required>
                      <?php
                        // Get admin roles.
                        $admin_roles = [$user_roles['sysadmin'], $user_roles['admin']];

                        // Enable options for System Admin and Admin only for users
                        // with System Admin role.
                        if ($user_roles['sysadmin'] != $user->getRole()) {
                          $user_roles = array_diff($user_roles, $admin_roles);
                        }

                        // Output data of each row.
                        foreach ($user_roles as $role) {
                          echo "<option value=\"$role\"";
                          if ($account->getRole() == $role) {
                            echo ' selected';
                          }
                          echo ">$role</option>";
                        }
                      ?>
                    </select>
                  <?php endif; ?>
                </div>
              </div>

            </section>

            <!-- Error -->
            <section id="error" class="col-sm-6 col-xs-12
              <?php if ($error_hidden): ?>
                hidden
              <?php endif; ?>
            ">
              <div class="alert alert-danger col-xs-12">
                <p id="error_message">
                  <?php if (isset($error_message)): ?>
                    <?php echo $error_message; ?>
                  <?php endif; ?>
                </p>
              </div>
            </section>

            <!-- Submit Buttons -->
            <section id="buttons" class="col-sm-6 col-xs-12">
              <div class="btn-group btn-group-justified" role="group">
                <div class="btn-group" role="group">
                  <button type="submit" id="btn_save" name="btn_save" class="btn btn-success">Save</button>
                </div>
                <?php if (1 != $uid): ?>
                  <div class="btn-group" role="group">
                    <button type="submit" id="btn_delete" name="btn_delete" class="btn btn-danger">Delete</button>
                  </div>
                <?php endif; ?>
                <div class="btn-group" role="group">
                  <button type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-default">Cancel</button>
                </div>
              </div>
            </section>
          </form>

        </div><!-- /.row -->
      <?php endif; ?>
    </div><!-- /.container -->

    <!-- Footer -->
    <?php include('includes/footer.php'); ?>

    <!-- Scripts -->
    <?php include('includes/incl-js.html'); ?>
    <script>
      $(document).ready(function(){

        // Change Email field if JS is enabled.
        $('#field_email_no_js').html('');
        $('#field_email_js').removeClass('hidden');

        // Enable manual editing of email
        $('#btn_edit_email').click(function() {
          $('#email').removeAttr('disabled');
          $('#email').parent().removeClass('input-group');
          $('#btn_edit_email').addClass('hidden');
        });

        $('#btn_save, #btn_delete').click(function(e){
          //Stop the form from submitting itself to the server.
          e.preventDefault();

          // Check which button was clicked and assign request type.
          if (this.id == 'btn_save') {
            var request_type = 'update';
          }
          else if (this.id == 'btn_delete') {
            var request_type = 'delete';
          }

          // Assign input values to variables.
          var uid = "<?php echo $uid; ?>";
          var firstname = $('#firstname').val();
          var lastname = $('#lastname').val();
          var email = $('#email').val();
          var role = $('#role').val();

          var data = {
            request_type: request_type,
            uid: uid,
            firstname: firstname,
            lastname: lastname,
            email: email,
            role: role
          };

          // Pass data to ajax form.
          $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'includes/edit-user.php',
            data: {
              json_data: JSON.stringify(data)
            },
            success: function(result) {
              // No errors.
              if (true == result.success) {
                $(location).attr('href', 'users.php');
              }
              // Error occurred.
              else {
                errors = result.errors;
                messages = '';
                // Add breaks between each message.
                for (var key in errors.messages) {
                  messages += errors.messages[key] + '<br>';
                }
                // Adds/removes .has-error from fields.
                for (var key in errors.fields) {
                  if (errors.fields[key]) {
                    $('#' + key).parent().addClass('has-error');
                  }
                  else {
                    $('#' + key).parent().removeClass('has-error');
                  }
                }
                // Display messages.
                $('#error').removeClass('hidden');
                $('#error_message').html(messages);
              }
            }
          });
        });

        $('#btn_cancel').click(function(e){
          // Return to Users without saving.
          $(location).attr('href', 'users.php');
        });
      });
    </script>

  </body>
</html>
