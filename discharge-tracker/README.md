This README was written by https://gitlab.com/sunnz.

## Config

Create ``config.php`` from ``config-sample.php`` (located in ``includes/config``).

    cp config-sample.php config.php

The defaults from ``config-sample.php`` should work for testing purposes.

## Requirements

* PHP-PDO.
* PHP 5.5 or higher.
  * PHP 5.6 and 7.1 are used in development.
* PDO-SQLite may be required on some systems that don't come with PHP-PDO package.

### macOS

Please install PHP 5.6 or PHP 7, and PHP-PDO via Homebrew.

Then run php server in the root of this project's directory:

    php -S 127.0.0.1:9999

### Docker

Docker can be used if you can't get PHP 5.5 or higher on your system.

    chmod 0775 includes/database
    chmod +s includes/database
    sudo chown 33 includes/database
    docker run --rm -it -p 9999:80 -v "$PWD":/var/www/html php:7-apache

Then open up http://127.0.0.1:9999/includes/install.php in your browser.

## Timezone

You may get timezone warnings if you are installing PHP for the first time.

The warnings come from the design of PHP, which you can fix by supplying a timezone for ``date.timezone`` in your PHP setting.

On macOS where you have installed PHP 5.6 via Homebrew, create a file in ``/local/etc/php/5.6/conf.d/timezone.ini``.

On CentOS/RHEL 7 and similiar Linux distro, create a file in ``/etc/php.d/timezone.ini``.

Put the following in to ``timezone.ini``: (replace "Australia/Melbourne" with your timezone.)

    [Date]
    ; Defines the default timezone used by the date functions
    ; http://php.net/date.timezone
    date.timezone = Australia/Melbourne

Then restart your PHP server and the warnings should be fixed. If you used the above command (``php -S 127.0.0.1:9999``) to start a local PHP server, just press ``CTRL-C`` to stop your PHP server, then restart by entering the command (``php -S 127.0.0.1:9999``) again.
