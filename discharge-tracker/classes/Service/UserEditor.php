<?php

namespace Service;

class UserEditor extends UserLoader {

  /**
   * Create new user in database.
   */
  public function createUser($uid, $firstname, $lastname, $role, $email) {
    $query = "INSERT INTO users (uid, firstname, lastname, role, email) VALUES (:uid, :firstname, :lastname, :role, :email)";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':uid', $uid);
    $stmt->bindParam(':firstname', $firstname);
    $stmt->bindParam(':lastname', $lastname);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':email', $email);
    $stmt->execute();
    return;
  }

  /**
   * Update user information in database.
   */
  public function updateUser($uid, $firstname, $lastname, $role, $email) {
    $query = "UPDATE users SET firstname = :firstname, lastname = :lastname, role = :role, email = :email WHERE uid = :uid";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':firstname', $firstname);
    $stmt->bindParam(':lastname', $lastname);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':uid', $uid);
    $stmt->execute();
    return;
  }

  /**
   * Delete user in database.
   */
  public function deleteUser($uid) {
    $query = "DELETE FROM users WHERE uid = :uid";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':uid', $uid);
    $stmt->execute();
    return;
  }

}
