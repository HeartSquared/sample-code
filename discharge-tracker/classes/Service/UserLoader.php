<?php

namespace Service;

use Model\User;

class UserLoader {

  const ROLE_SYSTEM_ADMIN = 'System Admin';
  const ROLE_ADMIN = 'Admin';
  const ROLE_MANAGER = 'Manager';
  const ROLE_PHARMACIST = 'Pharmacist';
  const ROLE_TECH = 'Technician';

  protected $pdo;

  public function __construct(\PDO $pdo) {
    $this->pdo = $pdo;
  }

  /**
   * Get list of user roles.
   * @return array.
   */
  public static function getUserRoles() {
    return $array = [
      'sysadmin' => self::ROLE_SYSTEM_ADMIN,
      'admin' => self::ROLE_ADMIN,
      'manager' => self::ROLE_MANAGER,
      'pharmacist' => self::ROLE_PHARMACIST,
      'tech' => self::ROLE_TECH,
    ];
  }

  /**
   * Gets single user by uid.
   * @return User.
   */
  public function getUserById($uid) {
    return $this->getSingleUser($uid, 'uid');
  }

  /**
   * @return User.
   */
  private function getSingleUser($value, $key) {
    $userArray = $this->querySingleUser($value, $key);
    $user = $this->getUserFromArray($userArray);
    return $user;
  }

  /**
   * @return User.
   */
  private function getUserFromArray(array $userArray) {
    $user = new User($userArray['uid']);
    $user->setFirstName($userArray['firstname']);
    $user->setLastName($userArray['lastname']);
    $user->setRole($userArray['role']);
    $user->setEmail($userArray['email']);
    return $user;
  }

  /**
   * PDO query for single User.
   * @return array.
   */
  private function querySingleUser($value, $filter_key) {
    $query = "SELECT * FROM users WHERE $filter_key = :value";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':value', $value);
    $stmt->execute();
    $userArray = $stmt->fetch(\PDO::FETCH_ASSOC);
    return $userArray;
  }

}
