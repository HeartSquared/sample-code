<?php

namespace Service;

use Model\AbstractTable;
use Model\TableEntries;

class TableManager {

  private $pdo;

  public function __construct(\PDO $pdo) {
    $this->pdo = $pdo;
  }

  private function displayTableHeaders(AbstractTable $table, $sort_option, $sort_order = 'asc') {
    $columns = $table->getColumns();
    // Display each column heading.
    echo '<table class="table table-bordered"><thead><tr>';
    foreach ($columns as $col => $value) {
      echo '<th><button class="btn btn-link sort-option';
      if ($sort_option == $col) {
        echo ' sort-select';
      }
      echo '" id="sort-' . $col . '-' . $sort_order . '">
            <strong>' . $value . '</strong>
            <span id="' . $col . '-caret" class="caret"></span>
          </button></th>';
    }
    if ($table->displayReadOnly()) {
      echo '</tr></thead>';
    }
    else {
      echo '<th style="width:22px;"></th></tr></thead>';
    }
  }

  public function displayTableData(AbstractTable $table, $collection_type, $sort_option, $sort_order) {

    $result = $table->getData($sort_option, $sort_order);

    // Span one extra column for edit button.
    $colspan = count($table->getColumns());
    if (!$table->displayReadOnly()) {
      $colspan++;
    }

    if (count($result)) {
      // Output data of each row.
      foreach ($result as $row) {

        // Remove row id from array (used for edit button value).
        $id = array_pop($row);
        echo '<tr>';
        
        foreach ($row as $col => $value) {
          echo "<td>$row[$col]</td>";
        }
        if (!$table->displayReadOnly()) {
          if (isset($entry_cancelled) && $entry_cancelled) {
            echo '<td colspan="2">Discharge cancelled</td>';
          }
          // Edit button for row.
          echo '
            <td style="padding-bottom:0;">
              <form id="form_edit" method="post" action="includes/edit-forward.php">
                <input type="image" src="img/edit.png" style="height:22px;" alt="Edit" title="Edit">
                <input name="id" type="hidden" value="' . $id . '">
                <input name="type" type="hidden" value="' . $collection_type . '">
              </form>
              <span id="old_sort_option" class="hidden">' . $sort_option . '</span>
            </td>
          </tr>';
        }
      }
      // Row count.
      echo '<tr class="text-center"><td colspan="' . $colspan . '">Total ' . $collection_type . ': ' .count($result) . '</td></tr>';
    }
    else {
      echo '<tr><td colspan="' . $colspan . '">There are no ' . $collection_type . ' registered!</td></tr>';
    }
  }

  public function displayTable(AbstractTable $table, $collection_type, $sort_option, $sort_order = 'ASC') {
    $this->displayTableHeaders($table, $sort_option);
    echo '<tbody id="table">';
    $this->displayTableData($table, $collection_type, $sort_option, $sort_order);
    echo '</tbody></table>';
  }

  /**
   * Display a custom table view for Head Office users (excludes System Admins).
   *
   * @param TableEntries $table
   */
  public function displayHeadOfficeEntriesTable(TableEntries $table) {
    $site_loader = new SiteLoader($this->pdo);
    $sites = $site_loader->getAllSites();

    foreach ($sites as $site) {
      // Get array of row counts for site by criteria.
      $summary = $table->getSiteEntrySummary($site->getSid());
      $total_count = 0;

      echo '<div class="col-sm-6">';

      // Table head.
      echo '<table class="table table-bordered"><thead><tr><th colspan="2">';
      echo $site->getSiteName();
      echo '</th></tr></thead>';

      // Table body.
      echo '<tbody>';
      foreach ($summary as $criteria => $row_count) {
        echo '<tr><td>';
        if (EntryLoader::STATUS_INCOMPLETE == $criteria) {
          echo 'In Progress';
        }
        elseif ('medprof' == $criteria) {
          echo 'Medication Profile Requested';
        }
        else {
          echo $criteria;
        }
        echo "</td><td>$row_count</td></tr>";
        if ('medprof' != $criteria) {
          $total_count += $row_count;
        }
      }

      echo '<tr class="text-center"><td colspan="2">';
      echo "Total discharge entries: $total_count</td></tr>";
      echo '</tbody></table>';
      echo '</div>';
    }
  }

}
