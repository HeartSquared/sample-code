<?php

namespace Service;

use Model\TableUsers;
use Model\AbstractUserType;

class Container {

  private $dbConfig;
  private $pdo;

  private $credentialLoader;
  private $credentialEditor;
  private $userLoader;
  private $userEditor;
  private $tableManager;
  private $tableUsers;

  public function __construct(array $dbConfig) {
    $this->dbConfig = $dbConfig;
  }

  /**
   * @return PDO.
   */
  public function getPDO() {

    if ($this->pdo === NULL) {
      $this->pdo = new \PDO(
        $this->dbConfig['dsn']
      );
      $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
    return $this->pdo;
  }

  /**
   * @return CredentialLoader.
   */
  public function getCredentialLoader() {
    if ($this->credentialLoader === NULL) {
      $this->credentialLoader = new CredentialLoader($this->getPDO());
    }
    return $this->credentialLoader;
  }

  /**
   * @return CredentialEditor.
   */
  public function getCredentialEditor() {
    if ($this->credentialEditor === NULL) {
      $this->credentialEditor = new CredentialEditor($this->getPDO());
    }
    return $this->credentialEditor;
  }

  /**
   * @return UserLoader.
   */
  public function getUserLoader() {
    if ($this->userLoader === NULL) {
      $this->userLoader = new UserLoader($this->getPDO());
    }
    return $this->userLoader;
  }

  /**
   * @return UserEditor.
   */
  public function getUserEditor() {
    if ($this->userEditor === NULL) {
      $this->userEditor = new UserEditor($this->getPDO());
    }
    return $this->userEditor;
  }

  /**
   * @return TableManager.
   */
  public function getTableManager() {
    if ($this->tableManager === NULL) {
      $this->tableManager = new TableManager($this->getPDO());
    }
    return $this->tableManager;
  }

  /**
   * @return TableUsers.
   */
  public function getTableUsers(AbstractUserType $user) {
    if ($this->tableUsers === NULL) {
      $this->tableUsers = new TableUsers($this->getPDO(), $user);
    }
    return $this->tableUsers;
  }

}
