<?php

namespace Service;

class CredentialEditor extends CredentialLoader {

  /**
   * Update username in database.
   */
  public function createCredential($username, $user_type) {
    $query = "INSERT INTO credentials (username, password, user_type) VALUES (:username, :pass, :user_type)";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':username', $username);
    // @TODO Randomise password and/or config option.
    $stmt->bindValue(':pass', password_hash('welcome123', PASSWORD_DEFAULT));
    $stmt->bindParam(':user_type', $user_type);
    $stmt->execute();
    return;
  }

  /**
   * Update username in database.
   */
  public function updateUsername($uid, $username) {
    $query = "UPDATE credentials SET username = :username WHERE uid = :uid";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':uid', $uid);
    $stmt->execute();
    return;
  }

  /**
   * Update user password in database.
   */
  public function updatePassword($uid, $password) {
    $query = "UPDATE credentials SET password = :password WHERE uid = :uid";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':uid', $uid);
    $stmt->execute();
    return;
  }

  /**
   * Delete login credentials in database.
   */
  public function deleteCred($uid) {
    $query = "DELETE FROM credentials WHERE uid = :uid";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':uid', $uid);
    $stmt->execute();
    return;
  }

  /**
   * Check if username is already in use.
   * @return boolean.
   */
  public function checkUserExists($username, $uid = NULL) {
    if ($uid) {
      $query = "SELECT username FROM credentials WHERE username = :username AND uid != :uid";
    }
    else {
      $query = "SELECT username FROM credentials WHERE username = :username";
    }
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':username', $username);
    if ($uid) {
      $stmt->bindParam(':uid', $uid);
    }
    $stmt->execute();
    $result = $stmt->fetch(\PDO::FETCH_ASSOC);
    if ($result) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
