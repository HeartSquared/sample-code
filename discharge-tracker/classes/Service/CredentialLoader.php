<?php

namespace Service;

use Model\Credential;

class CredentialLoader {

  protected $pdo;

  public function __construct(\PDO $pdo) {
    $this->pdo = $pdo;
  }

  /**
   * Get login credentials from database.
   * @return Credential.
   */
  public function getCredential($username) {
    return $this->getSingleCredential($username, 'username');
  }

  /**
   * Calls getCredential().
   * @return Credential.
   */
  public function getCredentialById($uid) {
    return $this->getSingleCredential($uid, 'uid');
  }

  /**
   * @return Credential.
   */
  private function getSingleCredential($value, $key) {
    $credArray = $this->querySingleCredential($value, $key);
    if ($credArray) {
      $credential = $this->getCredentialFromArray($credArray);
    }
    else {
      return NULL;
    }
    return $credential;
  }

  /**
   * @return Credential.
   */
  private function getCredentialFromArray(array $credArray) {
    $credential = new Credential($credArray['username']);
    $credential->setUid($credArray['uid']);
    $credential->setPassword($credArray['password']);
    $credential->setUserType($credArray['user_type']);
    return $credential;
  }

  /**
   * PDO query for single Credential.
   * @return array.
   */
  private function querySingleCredential($value, $filter_key) {
    $query = "SELECT * FROM credentials WHERE $filter_key = :value";
    $stmt = $this->pdo->prepare($query);
    $stmt->bindParam(':value', $value);
    $stmt->execute();
    $credArray = $stmt->fetch(\PDO::FETCH_ASSOC);
    return $credArray;
  }

}
