<?php

namespace Model;

class User extends AbstractUserType {

  private $firstname;
  private $lastname;
  private $role;
  private $email;

  /**
   * GETTERS.
   */

  /**
   * @return string
   */
  public function getFirstName() {
    return $this->firstname;
  }

  /**
   * @return string
   */
  public function getLastName() {
    return $this->lastname;
  }

  /**
   * @return string
   */
  public function getRole() {
    return $this->role;
  }

  /**
   * @return string
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * SETTERS.
   */

  /**
   * @param string $firstname
   */
  public function setFirstName($firstname) {
    $this->firstname = $firstname;
  }

  /**
   * @param string $lastname
   */
  public function setLastName($lastname) {
    $this->lastname = $lastname;
  }

  /**
   * @param string $role
   */
  public function setRole($role) {
    $this->role = $role;
  }

  /**
   * @param string $email
   */
  public function setEmail($email) {
    $this->email = $email;
  }

}
