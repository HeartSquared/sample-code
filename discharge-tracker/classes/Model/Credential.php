<?php

namespace Model;

class Credential {

  private $uid;
  private $username;
  private $password;
  private $user_type;

  /**
   * @param string $username
   */
  public function __construct($username) {
    $this->username = $username;
  }

  /**
   * GETTERS.
   */

  /**
   * @return integer
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * @return string
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * @return string
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * @return string
   */
  public function getUserType() {
    return $this->user_type;
  }

  /**
   * SETTERS.
   */

  /**
   * @param integer $uid
   */
  public function setUid($uid) {
    $this->uid = $uid;
  }

  /**
   * @param string $password
   */
  public function setPassword($password) {
    $this->password = $password;
  }

  /**
   * @param string $user_type
   */
  public function setUserType($user_type) {
    $this->user_type = $user_type;
  }

}
