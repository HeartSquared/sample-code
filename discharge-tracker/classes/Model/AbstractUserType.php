<?php

namespace Model;

abstract class AbstractUserType {

  const USERTYPE_USER = 'user';

  protected $uid;

  /**
   * Construct User.
   * Require uid from Credential.
   * @param integer $uid
   */
  public function __construct($uid) {
    $this->uid = $uid;
  }

  /**
   * GETTERS.
   */

  /**
   * @return string
   */
  public function getUid() {
    return $this->uid;
  }

  abstract public function getRole();

}
