<?php

namespace Model;

use Model\AbstractUserType;
use Service\CredentialLoader;

abstract class AbstractTable {

  protected $pdo;
  protected $user;
  protected $credential;

  public function __construct(\PDO $pdo, AbstractUserType $user) {
    $this->pdo = $pdo;
    $this->user = $user;
    $credential_loader = new CredentialLoader($pdo);
    $this->credential = $credential_loader->getCredentialById($user->getUid());
  }

  abstract public function getColumns();
  abstract protected function getDataQuery($sort_option, $sort_order);
  abstract protected function applyParams($stmt);

  /**
   * Get database data to display in table.
   * @return array
   */
  public function getData($sort_option, $sort_order) {
    $query = $this->getDataQuery($sort_option, $sort_order);
    $stmt = $this->pdo->prepare($query);
    $this->applyParams($stmt);
    $stmt->execute();
    $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    if (method_exists($this, 'modifyResults')) {
      $results = $this->modifyResults($results);
    }
    return $results;
  }

  /**
   * Get keys required formatted for query statement.
   * @return string
   */
  protected function getKeys($columns) {
    $keys = implode(', ', array_keys($columns));
    return $keys;
  }

  /**
   * Checks if the table should be displayed as read only.
   * @return bool
   */
  public function displayReadOnly() {
    return FALSE;
  }

}
