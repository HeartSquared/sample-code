<?php

namespace Model;

class TableUsers extends AbstractTable {

  /**
   * Columns to display.
   * Key: Database column name.
   * Value: Table header.
   * @return array
   */
  public function getColumns() {
    $columns = [
      'firstname' => 'First Name',
      'lastname' => 'Last Name',
      'role' => 'User Role',
      'email' => 'Email / Username',
    ];
    return $columns;
  }

  /**
   * Get database query.
   * @return string
   */
  public function getDataQuery($sort_option, $sort_order) {
    $columns = $this->getColumns();
    $keys = $this->getKeys($columns);
    // Assign User Id to keys (hidden field for editing).
    $keys .= ', users.uid';
    $query = "SELECT $keys FROM users JOIN credentials ON users.uid = credentials.uid";
    $query .= " ORDER BY $sort_option $sort_order, lastname ASC";
    return $query;
  }

  /**
   * Apply parameters to query statement.
   * @param PDOStatement $stmt
   */
  public function applyParams($stmt) {
    return;
  }

}
