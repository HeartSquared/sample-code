<?php

require_once('includes/config/config.php');
require_once('classes/Model/AbstractUserType.php');
require_once('classes/Service/UserLoader.php');

use Model\AbstractUserType;
use Service\UserLoader;

try {
  if (!file_exists('includes/database')) {
    mkdir('includes/database', 0775, true);
  }

  /**
   * Create and connect to database.
   */
  $pdo = new PDO($config['database']['dsn']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  /**
   * Create CREDENTIALS table.
   * Collection of all user logins (users and wards).
   *
   * uid = User ID (match in USERS, SITES and WARDS tables).
   * username = Username for login.
   * user_type = User type.
   * password = Hashed password.
   */
  $pdo->exec("CREATE TABLE IF NOT EXISTS credentials (
    uid INTEGER PRIMARY KEY,
    username TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    user_type TEXT NOT NULL)");

  /**
   * Create USERS table.
   * Collection of pharmacy staff user profiles.
   *
   * uid = User ID.
   * firstname = User's first name.
   * lastname = User's last name.
   * role = User role (Admin, Manager, Pharmacist, Technician).
   * email = User's email address (will be their username).
   */
  $pdo->exec("CREATE TABLE IF NOT EXISTS users (
    uid INTEGER PRIMARY KEY,
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    role TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE)");

  // Create System Admin account.
  // Create login credentials.
  $insert_credential = "INSERT INTO credentials (uid, username, password, user_type) VALUES (:uid, :username, :password, :user_type)";
  $stmt = $pdo->prepare($insert_credential);
  $stmt->bindValue(':uid', 1);
  $stmt->bindParam(':username', $config['sysadmin']['email']);
  $stmt->bindValue(':password', password_hash('admin', PASSWORD_DEFAULT));
  $stmt->bindValue(':user_type', AbstractUserType::USERTYPE_USER);
  $stmt->execute();

  // User profile.
  $insert_user = "INSERT INTO users (uid, firstname, lastname, role, email) VALUES (:uid, :firstname, :lastname, :role, :email)";
  $stmt = $pdo->prepare($insert_user);
  $stmt->bindValue(':uid', 1);
  $stmt->bindParam(':firstname', $config['sysadmin']['firstname']);
  $stmt->bindParam(':lastname', $config['sysadmin']['lastname']);
  $stmt->bindValue(':role', UserLoader::ROLE_SYSTEM_ADMIN);
  $stmt->bindParam(':email', $config['sysadmin']['email']);
  $stmt->execute();

  // Close db connection.
  $pdo = NULL;

  echo '<html>';
  echo "<p>Install successful. System Admin password is 'admin'.</p>";
  echo '<p><a href="index.php">Return to index.php</a></p>';
  echo '</html>';
}
catch(PDOException $e) {
  // Print PDOException message.
  echo $e->getMessage();
}
