<?php
/* Cassandra Tam 2018 */

require_once('includes/common.php');
?>

<!DOCTYPE html>
<html>

<head>
    <?php require_once('includes/head.html'); ?>
    <title>Symbiote exercise</title>
</head>

<body>
    <div class="wrapper">
        <?php require_once('includes/nav.php'); ?>

        <!-- Page Content  -->
        <div id="content">
            <?php require_once('includes/nav-toggle.html'); ?>

            <h1>Home</h1>

            <?php if (!file_exists('database/db.sq3')): ?>
                <p>Database appears to be missing! <a href="install.php" class="btn-link">Click here to run the install script.</a></p>
            <?php else: ?>
                <?php if ($logged_in): ?>
                    <p>You are logged in! Would you like to <a href="add-page.php" class="btn-link">add a new page</a>?</p>
                <?php else: ?>
                    <p>Hi! Please <a href="login.php" class="btn-link">log in</a> :)</p>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <?php include('includes/incl-js.html'); ?>
</body>

</html>
