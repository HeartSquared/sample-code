# Symbiote developer exercise
Brief at https://github.com/symbiote/developer-exercise/blob/master/overview.md.

Used Bootstrapious's Bootstrap Sidebar as a base (https://bootstrapious.com/p/bootstrap-sidebar).

## Requirements

* PHP-PDO.
* PHP 5.5 or higher - may actually work for lower versions, although not tested.
  * PHP 7.3 used in development.
* PDO-SQLite may be required on some systems that don't come with PHP-PDO package.

### macOS

Please install PHP 5.5 or PHP 7, and PHP-PDO via Homebrew.

Then run php server in the root of this project's directory (ensure you have `router.php`):

    php -S 127.0.0.1:9999 router.php

If you have Apache set up, ensure you have `.htaccess` in the root of your directory.
