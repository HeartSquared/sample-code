<?php
/* Cassandra Tam 2018 */

require_once('includes/common.php');

$error_hidden = TRUE;
require_once('includes/login.php');

$username_value = isset($_POST['username']) ? $_POST['username'] : NULL;
?>

<!DOCTYPE html>
<html>

<head>
    <?php require_once('includes/head.html'); ?>
    <title>Symbiote exercise</title>
</head>

<body>
    <div class="wrapper">
        <?php require_once('includes/nav.php'); ?>

        <!-- Page Content  -->
        <div id="content">
            <?php require_once('includes/nav-toggle.html'); ?>

            <h1>Login</h1>

            <div class="row">
                <div class="col-xl-8 col-lg-10">

                    <!-- Error -->
                    <section id="error" class="
                        <?php if ($error_hidden): ?>
                            d-none
                        <?php endif; ?>
                    ">
                        <div class="alert alert-danger" role="alert">
                            <span id="error_message">
                                <?php if (isset($error_message)): ?>
                                    <?php echo $error_message; ?>
                                <?php endif; ?>
                            </span>
                        </div>
                    </section>

                    <form method="post" action="">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" aria-describedby="usernameHelp" placeholder="Enter username" value="<?php echo $username_value; ?>" required>
                            <small id="usernameHelp" class="form-text text-muted">Username is <em>admin</em></small>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" aria-describedby="passwordHelp" placeholder="Enter password" required>
                            <small id="usernameHelp" class="form-text text-muted">Password is <em>devwithcoffeeandpancakes</em></small>
                        </div>
                        <button type="submit" name="btn_login" class="btn btn-secondary">Submit</button>
                    </form>

                </div>
            </div>

        </div>
    </div>

    <?php include('includes/incl-js.html'); ?>
</body>

</html>
