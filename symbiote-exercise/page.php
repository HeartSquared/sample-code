<?php
/* Cassandra Tam 2018 */

require_once('includes/common.php');
$page_loader = $container->getPageLoader();

$slug = isset($_GET['slug']) ? $_GET['slug'] : NULL;
$page = $page_loader->getPageBySlug($slug);
?>

<!DOCTYPE html>
<html>

<head>
    <?php require_once('includes/head.html'); ?>
    <title>Symbiote exercise</title>
</head>

<body>
    <div class="wrapper">
        <?php require_once('includes/nav.php'); ?>

        <!-- Page Content  -->
        <div id="content">
            <?php require_once('includes/nav-toggle.html'); ?>

            <?php if ($logged_in): ?>
                <?php if (!empty($page)): ?>
                    <h1><?php echo $page->getName(); ?></h1>
                    <p><?php echo $page->getContent(); ?></p>
                    <a href="edit-page.php?id=<?php echo $page->getId(); ?>" class="btn btn-secondary">Edit page</a>
                <?php else: ?>
                    <h1>Error!</h1>
                    <p>Page does not exist!</p>
                <?php endif; ?>
            <?php else: ?>
                <?php require_once('includes/forbidden.html'); ?>
            <?php endif; ?>
        </div>
    </div>

    <?php include('includes/incl-js.html'); ?>
</body>

</html>
