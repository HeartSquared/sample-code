<?php
/* Cassandra Tam 2018 */

try {
    // Make the database folder if it doesn't exist.
    if (!file_exists('database')) {
        mkdir('database', 0775, true);
    }

    /**
     * Create and connect to database.
     */
    $pdo = new PDO('sqlite:database/db.sq3');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /**
     * Create CREDENTIALS table.
     * Collection of all user logins.
     *
     * uid = User ID.
     * username = Username for login.
     * password = Hashed password.
     */
    $pdo->exec("CREATE TABLE IF NOT EXISTS credentials (
        uid INTEGER PRIMARY KEY,
        username TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL)");

    /**
     * Create PAGES table.
     * Collection of all pages.
     *
     * id = Page ID.
     * name = Page name.
     * content = Page content.
     * slug = URL slug.
     */
    $pdo->exec("CREATE TABLE IF NOT EXISTS pages (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        content TEXT NOT NULL,
        slug TEXT NOT NULL UNIQUE)");

    // Create login credentials.
    $insert_credential = "INSERT INTO credentials (uid, username, password) VALUES (:uid, :username, :password)";
    $stmt = $pdo->prepare($insert_credential);
    $stmt->bindValue(':uid', 1);
    $stmt->bindValue(':username', 'admin');
    $stmt->bindValue(':password', password_hash('devwithcoffeeandpancakes', PASSWORD_DEFAULT));
    $stmt->execute();

    // Close db connection.
    $pdo = NULL;

    echo '<html>';
    echo "<p>Install successful!</p>";
    echo '<p><a href="index.php">Return to index.php</a></p>';
    echo '</html>';
}
catch(PDOException $e) {
    // Print PDOException message.
    echo '<html>';
    echo '<p>' . $e->getMessage() . '</p>';
    echo '<p><a href="index.php">Return to index.php</a></p>';
    echo '</html>';
}
