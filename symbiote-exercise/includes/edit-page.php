<?php
/* Cassandra Tam 2018 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        require_once('common.php');

        $js_enabled = TRUE;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], TRUE));
    } else {
        $js_enabled = FALSE;
        $post_data = trim_array_values($_POST);

        $post_data['id'] = $id;

        $field_ids = [
            'page_name',
            'page_content',
        ];

        // If the $post_data array doesn't have the field, set it to null.
        foreach ($field_ids as $id) {
            if (!isset($post_data[$id])) {
                $post_data[$id] = NULL;
            }
        }

        // Set request type depending on which button is clicked.
        if (isset($_POST['btn_save'])) {
            $post_data['request_type'] = 'update';
        } elseif (isset($_POST['btn_delete'])) {
            $post_data['request_type'] = 'delete';
        } elseif (isset($_POST['btn_cancel'])) {
            // Return to page if edit is cancelled.
            header("Location: {$page->getSlug()}");
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_save']) || isset($_POST['btn_delete']) || $js_enabled) {
    $page_editor = $container->getPageEditor();

    switch ($post_data['request_type']) {
        case 'update':

        // Generate URL slug from page name.
        $slug = create_slug_from_name($post_data['page_name']);

        $errors = [];
        $error_fields = [];

        // Fields to perform error checking on.
        // Key: Field id.
        // Value: Field label name for error message.
        $fields_to_validate = [
            'page_name' => 'Page Name',
            'page_content' => 'Content',
        ];

        foreach ($fields_to_validate as $field_id => $label) {
            // Reset each field to not have error class.
            $errors['fields'][$field_id] = FALSE;

            if (empty($post_data[$field_id])) {
                // Input is blank.
                $errors['messages'][] = $label . ' cannot be blank.';
                $error_fields[] = $field_id;
            } elseif ('page_name' == $field_id) {
                // Page name must have at least one alphanumeric character for slug.
                if (empty($slug)) {
                    $errors['messages'][] = $label . ' must contain at least one alphanumeric charater (for the URL slug).';
                    $error_fields[] = $field_id;
                }
                elseif (!empty($page_editor->getPageBySlug($slug))) {
                    if ($post_data['id'] !== $page_editor->getPageBySlug($slug)->getId()) {
                        $errors['messages'][] = 'There is an existing page with the same generated URL slug.';
                        $error_fields[] = $field_id;
                    }
                }
            }
        }

        // Give error class to fields with errors.
        foreach (array_unique($error_fields) as $error_field) {
            if (in_array($error_field, array_keys($fields_to_validate))) {
                $errors['fields'][$error_field] = TRUE;
            }
        }

        // Check if there are errors.
        if (!empty($errors['messages'])) {
            $result = array_merge(['success' => FALSE], ['errors' => $errors]);
        } else {
            // No errors. Update the page.
            $page_editor->updatePage($post_data['id'], $slug, $post_data['page_name'], $post_data['page_content']);

            $result = ['success' => TRUE, 'slug' => $slug];
        }
        break;

    case 'delete':
        $page_editor->deletePage($post_data['id']);
        $result = ['success' => TRUE];
        break;
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Successful.
        if (TRUE == $result['success']) {
            if ('update' == $post_data['request_type']) {
                header("Location: $slug");
            } elseif ('delete' == $post_data['request_type']) {
                header('Location: index.php');
            }
        } else {
            // Failed. Display errors.
            $error_hidden = FALSE;
            $error_message = '';
            foreach ($result['errors']['messages'] as $error) {
              $error_message .= "$error<br>";
            }
        }
    }
}
