<?php
/* Cassandra Tam 2018 */

define('PROJECT_ROOT', dirname(__DIR__) . '/');

// Start session.
if (PHP_SESSION_NONE == session_status()) { session_start(); }

if (!isset($_SESSION['logged_in'])) {
    $_SESSION['logged_in'] = FALSE;
}

// Get logged in status.
$logged_in = FALSE;
if (TRUE == $_SESSION['logged_in']) {
    $logged_in = TRUE;
}

require_once(PROJECT_ROOT . 'includes/functions.php');

// Autoload classes.
spl_autoload_register(function ($class) {
    $path = PROJECT_ROOT . 'classes/'. str_replace('\\', '/', $class) . '.php';

    if (file_exists($path)) {
        require $path;
    }
});

use Service\Container;
$container = new Container();
