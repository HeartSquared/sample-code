<?php
/* Cassandra Tam 2018 */

if (isset($_POST['btn_login'])) {
    $post_data = $_POST;

    $login_user = $post_data['username'];
    $login_pass = $post_data['password'];

    $credential = $container->getCredentialLoader()->getCredential($login_user);

    $invalid_login = FALSE;

    // Check credential.
    if (empty($credential)) {
        // User does not exist.
        $invalid_login = TRUE;
    } else {
        // User exists. Check password.
        $password = $credential->getPassword();

        // Password does not match.
        if (!password_verify($login_pass, $password)) {
            $invalid_login = TRUE;
        }
    }

    if ($invalid_login) {
        // Invalid login.
        $result = [
            'success' => FALSE,
            'error' => 'Invalid username or password.',
        ];
    } else {
        // Valid login.
        // Assign session variables.
        $_SESSION['logged_in'] = TRUE;

        $result = [
            'success' => TRUE,
        ];
    }

    if (TRUE == $result['success']) {
        // Successful.
        header('Location: index.php');
    } else {
        // Failed. Display errors.
        $error_hidden = FALSE;
        $error_message = $result['error'];
    }

}
