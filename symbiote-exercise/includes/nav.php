<?php
/* Cassandra Tam 2018 */

require_once('includes/common.php');
?>
<!-- Sidebar  -->
<nav id="sidebar">
    <div class="sidebar-header">
        <h3><a href="index.php">Symbiote Exercise</a></h3>
    </div>

    <ul class="list-unstyled components">
        <li>
            <a href="index.php">Home</a>
        </li>
        <?php if ($logged_in): ?>
            <?php
                $page_loader = $container->getPageLoader();
                $nav_pages = $page_loader->getAllPages();
            ?>
            <?php foreach ($nav_pages as $nav_page): ?>
                <li>
                    <a href="<?php echo $nav_page->getSlug(); ?>">
                        <?php echo $nav_page->getName(); ?>
                    </a>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>

    <ul class="list-unstyled CTAs">
        <?php if ($logged_in): ?>
            <li>
                <a href="add-page.php" class="add-page">Add page</a>
            </li>
            <li>
                <a href="logout.php" class="add-page">Logout</a>
            </li>
        <?php else: ?>
            <li>
                <a href="login.php" class="add-page">Login</a>
            </li>
        <?php endif; ?>
    </ul>
</nav>
