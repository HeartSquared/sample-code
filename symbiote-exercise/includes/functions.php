<?php
/* Cassandra Tam 2018 */

/**
 * Trim values and return the new array.
 *
 * @param array $array
 *
 * @return array
 */
function trim_array_values(array $array)
{
    $new_array = [];

    foreach ($array as $key => $value) {
        if (isset($value) && !is_null($value)) {
            // If there's a value, return it trimmed.
            $new_array[$key] = trim($value);
        } else {
            // No value, then set it to NULL.
            $new_array[$key] = NULL;
        }
    }

    return $new_array;
}

/**
 * Create URL slug from page name.
 *
 * @param string $name Page name.
 *
 * @return string Formatted URL slug.
 */
function create_slug_from_name($name)
{
    // Remove characters which are not alphanumeric or hyphens.
    $sanitised = preg_replace('/[^a-zA-Z0-9 ]/', '', $name);
    // Trim the page name.
    $trimmed = trim($sanitised);
    // Change spaces to hyphens.
    $hyphenated = preg_replace('/\s/', '-', $trimmed);
    // Make lowercase.
    $slug = strtolower($hyphenated);

    return $slug;
}
