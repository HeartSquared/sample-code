<?php

namespace Service;

class PageEditor extends PageLoader
{

    /**
     * Create new page in database.
     */
    public function createPage($slug, $name, $content)
    {
        $query = "INSERT INTO pages (slug, name, content) VALUES (:slug, :name, :content)";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':slug', $slug);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':content', $content);
        $stmt->execute();
        return;
    }

    /**
     * Update page information in database.
     */
    public function updatePage($id, $slug, $name, $content)
    {
        $query = "UPDATE pages SET slug = :slug, name = :name, content = :content WHERE id = :id";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':slug', $slug);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':content', $content);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return;
    }

    /**
     * Delete page in database.
     */
    public function deletePage($id)
    {
        $query = "DELETE FROM pages WHERE id = :id";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return;
    }

}
