<?php

namespace Service;

use Model\Page;

class PageLoader
{

    protected $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return Page[]
     */
    public function getAllPages()
    {
        $pagesArray = $this->queryAllPages();
        $pages = [];
        foreach ($pagesArray as $pageArray) {
            $page = $this->getPageFromArray($pageArray);
            $pages[] = $page;
        }
        return $pages;
    }

    /**
     * Get page details by slug from database.
     * @return Page.
     */
    public function getPageBySlug($slug)
    {
        return $this->getSinglePage($slug, 'slug');
    }

    /**
     * Get page details by id from database.
     * @return Page.
     */
    public function getPageById($id)
    {
        return $this->getSinglePage($id, 'id');
    }

    /**
     * @return Page.
     */
    private function getSinglePage($value, $key)
    {
        $pageArray = $this->querySinglePage($value, $key);
        if ($pageArray) {
            $page = $this->getPageFromArray($pageArray);
        } else {
            return NULL;
        }
        return $page;
    }

    /**
     * @return Page.
     */
    private function getPageFromArray(array $pageArray)
    {
        $page = new Page($pageArray['slug']);
        $page->setId($pageArray['id']);
        $page->setContent($pageArray['content']);
        $page->setName($pageArray['name']);
        return $page;
    }

    /**
     * PDO query for all Pages.
     * @return array.
     */
    private function queryAllPages()
    {
        $query = "SELECT * FROM pages";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $pagesArray = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $pagesArray;
    }

    /**
     * PDO query for single Page.
     * @return array.
     */
    private function querySinglePage($value, $filter_key)
    {
        $query = "SELECT * FROM pages WHERE $filter_key = :value";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        $pageArray = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $pageArray;
    }

}
