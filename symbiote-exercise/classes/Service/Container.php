<?php

namespace Service;

class Container
{

    private $pdo;
    private $credentialLoader;
    private $pageLoader;
    private $pageEditor;

    /**
     * @return PDO
     */
    public function getPDO()
    {
        if ($this->pdo === NULL) {
            $this->pdo = new \PDO('sqlite:' . PROJECT_ROOT . 'database/db.sq3');
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        return $this->pdo;
    }

    /**
     * @return CredentialLoader
     */
    public function getCredentialLoader()
    {
        if ($this->credentialLoader === NULL) {
            $this->credentialLoader = new CredentialLoader($this->getPDO());
        }
        return $this->credentialLoader;
    }

    /**
     * @return PageLoader
     */
    public function getPageLoader()
    {
        if ($this->pageLoader === NULL) {
            $this->pageLoader = new PageLoader($this->getPDO());
        }
        return $this->pageLoader;
    }

    /**
     * @return PageEditor
     */
    public function getPageEditor()
    {
        if ($this->pageEditor === NULL) {
            $this->pageEditor = new PageEditor($this->getPDO());
        }
        return $this->pageEditor;
    }

}
