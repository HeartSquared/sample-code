<?php
/* Cassandra Tam 2018 */

require_once('includes/common.php');

$error_hidden = TRUE;
require_once('includes/add-page.php');

$page_name_value = isset($_POST['page_name']) ? $_POST['page_name'] : NULL;
$page_content_value = isset($_POST['page_content']) ? $_POST['page_content'] : NULL;
?>

<!DOCTYPE html>
<html>

<head>
    <?php require_once('includes/head.html'); ?>
    <title>Symbiote exercise</title>
</head>

<body>
    <div class="wrapper">
        <?php require_once('includes/nav.php'); ?>

        <!-- Page Content  -->
        <div id="content">
            <?php require_once('includes/nav-toggle.html'); ?>

            <?php if ($logged_in): ?>
                <h1>Add Page</h1>

                <div class="row">
                    <div class="col-xl-8 col-lg-10">

                        <!-- Error -->
                        <section id="error" class="
                            <?php if ($error_hidden): ?>
                                d-none
                            <?php endif; ?>
                        ">
                            <div class="alert alert-danger" role="alert">
                                <span id="error_message">
                                    <?php if (isset($error_message)): ?>
                                        <?php echo $error_message; ?>
                                    <?php endif; ?>
                                </span>
                            </div>
                        </section>

                        <form id="form_add" method="post" action="">
                            <div class="form-group">
                                <label for="page_name">Page name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="page_name" name="page_name" aria-describedby="nameHelp" value="<?php echo $page_name_value; ?>" required>
                                <small id="nameHelp" class="form-text text-muted">Page name will be used to generate the URL slug - ensure it contains at least one alphanumeric character.</small>
                            </div>
                            <div class="form-group">
                                <label for="page_content">Content <span class="text-danger">*</span></label>
                                <textarea class="form-control" id="page_content" name="page_content" rows="8"><?php echo $page_content_value; ?></textarea>
                            </div>
                            <button type="submit" name="btn_add" class="btn btn-secondary">Submit</button>
                        </form>

                    </div>
                </div>
            <?php else: ?>
                <?php require_once('includes/forbidden.html'); ?>
            <?php endif; ?>

        </div>
    </div>

    <!-- Scripts -->
    <?php include('includes/incl-js.html'); ?>
    <script>
        $(document).ready(function() {
            $('#form_add').submit(function(e) {
                //Stop the form from submitting itself to the server.
                e.preventDefault();
                // Assign input values to variables.
                var page_name = $('#page_name').val();
                var page_content = $('#page_content').val();

                var data = {
                    page_name: page_name,
                    page_content: page_content
                };

                // Pass data to ajax form.
                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: 'includes/add-page.php',
                    data: {
                      json_data: JSON.stringify(data)
                    }
                })
                .done(function(result) {
                    // No errors.
                    if (true == result.success) {
                        $(location).attr('href', result.slug);
                    }
                    // Error occurred.
                    else {
                        errors = result.errors;
                        messages = '';
                        // Add breaks between each message.
                        for (var key in errors.messages) {
                            messages += errors.messages[key] + '<br>';
                        }
                        // Adds/removes .is-invalid from fields.
                        for (var key in errors.fields) {
                            if (errors.fields[key]) {
                                $('#' + key).addClass('is-invalid');
                            }
                            else {
                                $('#' + key).removeClass('is-invalid');
                            }
                        }
                        // Display messages.
                        $('#error').removeClass('d-none');
                        $('#error_message').html(messages);
                    }
                });
            });
        });
    </script>
</body>

</html>
