<?php
// router.php
if (preg_match('/\/[a-z0-9-]+(?!\.php)$/', $_SERVER['REQUEST_URI'])) {
    $_GET['slug'] = trim($_SERVER['REQUEST_URI'], '/');
    include('page.php');
} else {
    return false;
}
?>
